#pragma once

#include "tree_node_base.h"
#include <type_traits>
#include <utility>

template<typename Key, typename Mapped>
class tree_node : public tree_node_base {

public:

    using key_type = Key;
    using mapped_type = Mapped;
    using value_type = std::pair<const key_type, mapped_type>;

private:
	
    value_type value;

public:

    template<typename... Args>
    constexpr tree_node(Args&&... args)
    noexcept(std::is_nothrow_constructible_v<value_type, Args&&...>)
    : value(std::forward<Args>(args)...) { }

    value_type& get_value() noexcept {
        return value;
    }

    const value_type& get_value() const noexcept {
        return value;
    }

    const key_type& get_key() const noexcept {
        return value.first;
    }

};
