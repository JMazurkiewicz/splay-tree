#pragma once

#include <functional>
#include <stdexcept>
#include "tree_node.h"
#include <type_traits>
#include <utility>

template<typename Key, typename Mapped, typename Compare = std::less<Key>>
class splay_tree {

public:

    using key_type = Key;
    using mapped_type = Mapped;

    using value_type = std::pair<const key_type, mapped_type>;
    using reference = value_type&;
    using size_type = std::size_t;
    
    using key_compare = Compare;

private:

    using node_type = tree_node<key_type, mapped_type>;
    using node_ptr = node_type*;
    using node_base_ptr = tree_node_base*;

    node_ptr root;
    size_type tree_size;
    key_compare compare;

    template<typename... Args>
    [[nodiscard]] static node_ptr allocate_node(Args&&... args) {
        return new node_type(std::forward<Args>(args)...);
    }

    static void deallocate_node(node_base_ptr node) noexcept {
        delete static_cast<node_ptr>(node);
    }

public:

    splay_tree() noexcept(std::is_nothrow_default_constructible_v<key_compare>)
    : root{nullptr}, tree_size{0}, compare() { }

    splay_tree(const splay_tree&) = delete;
    splay_tree& operator=(const splay_tree&) = delete;

    ~splay_tree() {
        if(!empty()) {
            deallocate_subtree(root);
        }
    }

private:

    void deallocate_subtree(node_base_ptr subtree) noexcept {

        if(subtree->has_left_child()) {
            deallocate_subtree(subtree->get_left_child());
        }

        if(subtree->has_right_child()) {
            deallocate_subtree(subtree->get_right_child());
        }

        deallocate_node(subtree);

    }

public:

    size_type size() const noexcept {
        return tree_size;
    }

    bool empty() const noexcept {
        return size() == 0;
    }

    key_compare key_comp() const noexcept(std::is_nothrow_copy_constructible_v<key_compare>) {
        return compare;
    }

    bool insert(const value_type& value) {
        return emplace(value);
    }

    bool insert(const key_type& key, const mapped_type& value) {
        return emplace(key, value);
    }

    template<typename... Args>
    bool emplace(Args&&... args) {

        const node_ptr new_node = allocate_node(std::forward<Args>(args)...);

        if(empty()) {
            root = new_node;
        } else if(try_insert(root, new_node)) {
            splay(new_node);
        } else {
            deallocate_node(new_node);
            return false;
        }

        ++tree_size;
        return true;

    }

private:

    bool try_insert(node_base_ptr subtree, node_base_ptr new_node) {

        bool insertion_succeeded = false;

        auto&& new_node_key = static_cast<node_ptr>(new_node)->get_key();
        auto&& subtree_key = static_cast<node_ptr>(subtree)->get_key();

        if(compare(new_node_key, subtree_key)) {
            
            if(subtree->has_left_child()) {
                insertion_succeeded = try_insert(subtree->get_left_child(), new_node);
            } else {
                subtree->attach_left_child(new_node);
                insertion_succeeded = true;
            }

        } else if(compare(subtree_key, new_node_key)) {

            if(subtree->has_right_child()) {
                insertion_succeeded = try_insert(subtree->get_right_child(), new_node);
            } else {
                subtree->attach_right_child(new_node);
                insertion_succeeded = true;
            }

        }

        return insertion_succeeded;

    }

public:

    reference get_root_value() {

        if(empty()) {
            throw std::out_of_range{"splay_tree::get_root_value(): tree is empty"};
        }

        return root->get_value();

    }

    reference find(const key_type& key) {

        if(node_ptr found = find_at(root, key); found != nullptr) {
            splay(found);
            return found->get_value();
        }

        throw std::out_of_range{"splay_tree::find(): element doesn\'t exist"};

    }

private:

    node_ptr find_at(node_base_ptr subtree, const key_type& key) const {

        if(subtree != nullptr) {
            
            auto&& subtree_key = static_cast<node_ptr>(subtree)->get_key();

            if(compare(key, subtree_key)) {
                subtree = find_at(subtree->get_left_child(), key);
            } else if(compare(subtree_key, key)) {
                subtree = find_at(subtree->get_right_child(), key);
            }

        }
        
        return static_cast<node_ptr>(subtree);

    }

    void splay(node_base_ptr node) {
        
        while(node != root) {

            if(const node_base_ptr parent = node->get_parent(); parent == root) {
                zig(node);
            } else if(node->is_left_child() == parent->is_left_child()) {
                zig_zig(node);
            } else {
                zig_zag(node);
            }

        }

    }

    void zig(node_base_ptr node) {
        if(node->is_left_child()) {
            rotate_right(node);
        } else {
            rotate_left(node);
        }
	}

    void zig_zig(node_base_ptr node) {

        const node_base_ptr parent = node->get_parent();

        if(node->is_left_child()) {
            rotate_right(parent);
            rotate_right(node);
        } else {
            rotate_left(parent);
            rotate_left(node);
        }

	}

    void zig_zag(node_base_ptr node) {

        if(node->is_left_child()) {
            rotate_right(node);
            rotate_left(node);
        } else {
            rotate_left(node);
            rotate_right(node);
        }

    }

    void rotate_right(node_base_ptr node) {

        const node_base_ptr parent = node->get_parent();
        const node_base_ptr grandparent = parent->get_parent();
        const bool is_left_subtree = parent->is_left_child();
        
        parent->attach_left_child(node->get_right_child());
        node->attach_right_child(parent);

        if(is_left_subtree) {
            node->attach_on_left(grandparent);
        } else {
            node->attach_on_right(grandparent);
        }

        if(node->get_parent() == nullptr) {
            root = static_cast<node_ptr>(node);
        }

    }

    void rotate_left(node_base_ptr node) {

        const node_base_ptr parent = node->get_parent();
        const node_base_ptr grandparent = parent->get_parent();
        const bool is_left_subtree = parent->is_left_child();

        parent->attach_right_child(node->get_left_child());
        node->attach_left_child(parent);        

        if(is_left_subtree) {
            node->attach_on_left(grandparent);
        } else {
            node->attach_on_right(grandparent);
        }

        if(node->get_parent() == nullptr) {
            root = static_cast<node_ptr>(node);
        }

    }

};
