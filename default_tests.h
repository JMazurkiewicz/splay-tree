#pragma once

#include "splay_tree.h"
#include <ctime>

time_t test_insert(splay_tree<std::string, int>& t, std::string klucz, int wartosc){

    time_t start = clock();
    t.insert(klucz, wartosc);
    time_t end = clock();
    return difftime(end, start)/CLOCK_PER_SEC;

}

time_t test_search(splay_tree<std::string, int>& t, std::string klucz){
    
    time_t start = clock();
    t.find(klucz);
    time_t end = clock();
    return difftime(end, start)/CLOCK_PER_SEC;

}

void test_spay_tree (vector<std::pair<std::string, int>>){
    std::cout<<"Test drzewa splay\n";
    // t - puste drzewo
    splay_tree<std::string,int> t;
    for (int i=0; i < 1000; ++i){
        t.insert(vec[i].first,vec[i].second)     
    }
    
// liczba elementow = 1000
    
    std::cout<<"Liczba elementow drzewa:\t"<<t.getSize<<endl;  //rozmiar
    std::cout<<"Czas wstawienia slowa do slownika:\t"<<test_insert(t, k1, w1);
    std::cout<<"Czas wyszukania slowa:\t"<<test_search(t, k1, w1);

    std::cout<<"\n\n";
    
    for (int i=1000; i < 2000; ++i){
        t.insert(vec[i].first,vec[i].second)     
    }

// liczba elementow = 2000

    std::cout<<"Liczba elementow drzewa:\t"<<t.getSize<<endl;  //rozmiar
    std::cout<<"Czas wstawienia slowa do slownika:\t"<<test_insert(t, k1, w1);
    std::cout<<"Czas wyszukania slowa:\t"<<test_search(t, k1, w1);

    std::cout<<"\n\n";

    for (int i=2000; i < 10000; ++i){
        t.insert(vec[i].first,vec[i].second)     
    }

// liczba elementow = 10000

    std::cout<<"Liczba elementow drzewa:\t"<<t.getSize<<endl;  //rozmiar
    std::cout<<"Czas wstawienia slowa do slownika:\t"<<test_insert(t, k1, w1);
    std::cout<<"Czas wyszukania slowa:\t"<<test_search(t, k1, w1);

    std::cout<<"\n\n";

    for (int i=10000; i < 30000; ++i){
        t.insert(vec[i].first,vec[i].second)     
    }

// liczba elementow = 30000

    std::cout<<"Liczba elementow drzewa:\t"<<t.getSize<<endl;  //rozmiar
    std::cout<<"Czas wstawienia slowa do slownika:\t"<<test_insert(t, k1, w1);
    std::cout<<"Czas wyszukania slowa:\t"<<test_search(t, k1, w1);

    std::cout<<"\n\n";

    for (int i=30000; i < vec.size(); ++i){
        t.insert(vec[i].first,vec[i].second)     
    }

// liczba elementow = 10000

    std::cout<<"Liczba elementow drzewa:\t"<<t.getSize<<endl;  //rozmiar
    std::cout<<"Czas wstawienia slowa do slownika:\t"<<test_insert(t, k1, w1);
    std::cout<<"Czas wyszukania slowa:\t"<<test_search(t, k1, w1);

}


