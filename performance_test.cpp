#include <algorithm>
#include <chrono>
#include <functional>
#include <iostream>
#include <map>
#include <random>
#include "splay_tree.h"
#include <string>
#include <string_view>
#include <vector>
#include <utility>

// Standard: C++17 (-std=c++17 | /std:c++17)

using default_duration = std::chrono::duration<double, std::milli>;
inline constexpr std::string_view default_duration_suffix = "ms";

template<typename Duration = default_duration, typename Callable>
[[nodiscard]] Duration invoke_and_measure_time(Callable&& callable) {
    
    const auto start = std::chrono::high_resolution_clock::now();
    std::invoke(std::forward<Callable>(callable));
    const auto stop = std::chrono::high_resolution_clock::now();

    const auto result = stop - start;
    return std::chrono::duration_cast<Duration>(result);

}

template<typename T>
class counted_less {

public:

    bool operator()(const T& left, const T& right) const {
        ++comparison_count;
        return left < right;
    }

    std::size_t get_comparison_count() const {
        return comparison_count;
    }

private:

    mutable std::size_t comparison_count = 0;

};

using test_vector = std::vector<std::pair<int, int>>;

template<typename Container>
class test {

private:

    Container container;
    std::string test_name;

public:

    test(std::string_view test_name)
    : test_name(test_name) { }

    test(const test&) = delete;
    test& operator=(const test&) = delete;

    void perform(const test_vector& data) {

        if(!data.empty()) {        
        
            std::cout << "Testing " << test_name << ", size = " << data.size() << '\n';
            measure_insertion_time(data);
            measure_lookup_time(data);
            measure_one_element_lookup_time(data);
            display_total_comparison_count();
        
        } else {

            std::cerr << "Couldn\'t perform the test (test_vector is empty).\n";

        }

    }

private:

    void measure_insertion_time(const test_vector& data) {

        const default_duration time = invoke_and_measure_time([&] {

            for(auto&& e : data) {
                container.insert(e);
            }

        });

        std::cout << "  invoking insert function " << data.size() << " times: ";
        std::cout << time.count() << default_duration_suffix << '\n';

    }

    void measure_lookup_time(const test_vector& data) {

        const default_duration time = invoke_and_measure_time([&] {

            for(auto it = data.rbegin(); it != data.rend(); ++it) {
                static_cast<void>(container.find(it->first));
            }

        });

        std::cout << "  invoking lookup function " << data.size() << " times: ";
        std::cout << time.count() << default_duration_suffix << '\n';

    }

    void measure_one_element_lookup_time(const test_vector& data) {

        int key = data.back().first;

        const default_duration time = invoke_and_measure_time([&] {

            for(test_vector::size_type i = 0; i < data.size(); ++i) {
                static_cast<void>(container.find(key));
            }

        });

        std::cout << "  invoking lookup function " << data.size();
        std::cout << " times (key: " << key << "): ";
        std::cout << time.count() << default_duration_suffix << '\n';

    }

    void display_total_comparison_count() {
        std::cout << "  total count of comparisons: ";
        std::cout << container.key_comp().get_comparison_count() << '\n';
    }

};

test_vector generate_test_vector(std::size_t size) {

    test_vector data(size);

    std::generate(data.begin(), data.end(), [i = 0]() mutable {
        ++i;
        return std::make_pair(i, i);
    });

    static std::mt19937 generator{std::random_device{}()};
    std::shuffle(data.begin(), data.end(), generator);
    
    return data;

}

void unit_test(std::size_t data_size) {

    const test_vector data = generate_test_vector(data_size);

    using map = std::map<int, int, counted_less<int>>;
    using splay = ::splay_tree<int, int, counted_less<int>>;

    {
        test<map> map_test("std::map<int, int>");
        map_test.perform(data);
    }

    {
        test<splay> splay_test("::splay_tree<int, int>");
        splay_test.perform(data);
    }

    std::cout << std::endl;

}

int main(int argc, char* argv[]) {

    struct pretty_number : std::numpunct<char> {
        char do_thousands_sep() const override { return '\''; }
        std::string do_grouping() const override { return "\x03"; }
        char do_decimal_point() const override { return ','; }
    };

    std::ios_base::sync_with_stdio(false);
    std::cout.imbue(std::locale{std::cout.getloc(), new pretty_number});

    if(argc <= 1) {
        
        unit_test(1'000);
        unit_test(10'000);
        unit_test(100'000);
        unit_test(1'000'000);
        unit_test(10'000'000);
    
    } else {

        try {

            for(int i = 1; i < argc; ++i) {
                const std::size_t test_size = std::stoull(argv[i]);
                unit_test(test_size);
            }

        } catch(std::exception& e) {

            std::cerr << "invalid input: \"" << e.what() << "\"\n";
            std::cerr << "execution canceled...\n";

        }

    }

}