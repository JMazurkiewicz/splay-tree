#pragma once

class tree_node_base {

private:

    tree_node_base* left;
    tree_node_base* right;
    tree_node_base* parent;

public:

    constexpr tree_node_base() noexcept
    : left{nullptr}, right{nullptr}, parent{nullptr} { }

    tree_node_base* get_parent() noexcept {
        return parent;
    }

    const tree_node_base* get_parent() const noexcept {
        return parent;
    }

    tree_node_base* get_left_child() noexcept {
        return left;
    }

    const tree_node_base* get_left_child() const noexcept {
        return left;
    }

    tree_node_base* get_right_child() noexcept {
        return right;
    }

    const tree_node_base* get_right_child() const noexcept {
        return right;
    }

    bool has_parent() const noexcept {
        return get_parent() != nullptr;
    }

    bool has_left_child() const noexcept {
        return get_left_child() != nullptr;
    }

    bool has_right_child() const noexcept {
        return get_right_child() != nullptr;
    }

    bool is_left_child() const noexcept {
        return has_parent() && get_parent()->left == this;
    }

    bool is_right_child() const noexcept {
        return has_parent() && get_parent()->right == this;
    }

    void attach_left_child(tree_node_base* new_left) noexcept {
        left = new_left;
        if(new_left != nullptr) {
            new_left->parent = this;
        }
    }

    void attach_right_child(tree_node_base* new_right) noexcept {
        right = new_right;
        if(new_right != nullptr) {
            new_right->parent = this;
        }
    }

    void attach_on_left(tree_node_base* new_parent) noexcept {
        parent = new_parent;
        if(new_parent != nullptr) {
            new_parent->left = this;
        }
    }

    void attach_on_right(tree_node_base* new_parent) noexcept {
        parent = new_parent;
        if(new_parent != nullptr) {
            new_parent->right = this;
        }
    }

};
