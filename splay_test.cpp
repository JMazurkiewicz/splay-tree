#include <fstream>
#include <iostream>
#include <random>
#include "splay_tree.h"
#include <string>
#include <utility>
#include <vector>

namespace {
	std::mt19937 generator{std::random_device{}()};
}

class splay_test {

private:

	using tree_type = splay_tree<std::string, int>;

	std::vector<std::string> keys;
	tree_type tree;
	std::size_t lookup_test_count;

public:

	splay_test(std::size_t lookup_test_count)
	: lookup_test_count{lookup_test_count} { }

	void perform() {
		generate_keys();
		insertion_test();
		lookup_test();
	}

private:

	void generate_keys() {

		std::ifstream file("pan-tadeusz.txt");

		for(std::string key; file >> key; ) {
			keys.push_back(std::move(key));
		}

	}

	void insertion_test() {

		std::uniform_int_distribution distr(100, 999);

		for(auto&& key : std::as_const(keys)) {
		
			const tree_type::value_type value(key, distr(generator));
			const bool success = tree.insert(value);

			if(success && value != tree.get_root_value()) {
				std::cerr << "insertion test failed:\n";
				std::cerr << "    " << "root: " << tree.get_root_value().first;
				std::cerr << "    " << "\nis not equal to inserted element\n";
				std::cerr << "    " << value.first << '\n';
			}

		}

	}

	void lookup_test() {

		std::uniform_int_distribution<std::size_t> distr(0, keys.size()-1);

		for(std::size_t i = 0; i < lookup_test_count; ++i) {
			try {

				auto&& key = keys[distr(generator)];
				auto&& value = tree.find(key);

				if(value != tree.get_root_value()) {
					std::cerr << "lookup test failed:\n";
					std::cerr << "    " << "root: " << tree.get_root_value().first;
					std::cerr << "    " << "\nis not equal to found element\n";
					std::cerr << "    " << value.first << '\n';
				}
					
			} catch(std::exception& e) {
				std::cerr << "unexpected error: " << e.what() << '\n';
			}
		}

	}

};

int main(int argc, char* argv[]) {

	std::size_t lookup_test_count = 1'024;

	if(argc > 1) {

		try {
			lookup_test_count = std::stoull(argv[1]);
		} catch(std::exception& e) {
			std::cerr << "Invalid program argument: \"";
			std::cerr << e.what() << "\"\n";
		}

	}

	splay_test test{lookup_test_count};
	test.perform();

}
